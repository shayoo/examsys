create database exam
use exam
create table tblbat(BID int identity, Batch varchar(max) not null, DOS varchar(max) not null, NOS varchar(10) not null, DOE varchar(max) null)
create table tblstu(Roll varchar(max) not null, SName varchar(30) not null, Batch varchar(max) not null, Course varchar(30) not null, Phone varchar(30) not null)
create table tblexam(Roll varchar(max) not null, ExamDone varchar(30) null, ExamDate varchar(30) null, IT1 varchar(10) null, IT2 varchar(10) null, IT3 varchar(10) null, Project varchar(10) null, Transcript varchar(10) null)
select * from tblbat
select * from tblstu
select * from tblexam
insert into tblbat values('cf','23','23','')
truncate table tblbat
truncate table tblstu
truncate table tblexam